var myApp = angular.module("myApp", []);

myApp.controller("myController", function ($scope) {

    $scope.persons = [
        {
            "firstName": "John",
            "lastName": "Smith",
            "age": 25,
            "address": {
                "streetAddress": "21 2nd Street",
                "city": "New York",
                "state": "NY",
                "postalCode": "10021"
            },
            "phoneNumber": [
                {
                    "type": "home",
                    "number": "212 555-1234"
                },
                {
                    "type": "fax",
                    "number": "646 555-4567"
                }
            ]
        },
        {
            "firstName": "Simona",
            "lastName": "Morasca",
            "age": 22,
            "address": {
                "streetAddress": "3 Mcauley Dr",
                "city": "Ashland",
                "state": "OH",
                "postalCode": "44805"
            },
            "phoneNumber": [
                {
                    "type": "home",
                    "number": "419-503-2484"
                },
                {
                    "type": "fax",
                    "number": "419-800-6759"
                }
            ]
        },
        {
            "firstName": "Josephine",
            "lastName": "Darakjy",
            "age": 33,
            "address": {
                "streetAddress": "4 B Blue Ridge Blvd",
                "city": "Brighton",
                "state": "MI",
                "postalCode": "48116"
            },
            "phoneNumber": [
                {
                    "type": "home",
                    "number": "973-605-6492"
                },
                {
                    "type": "fax",
                    "number": "602-919-4211"
                }
            ]
        }
    ];
    $scope.predicate = 'firstName';

    this.sort = function(field){

        var th = $(".contact_table").find("th");
        th.find("div").removeClass("dropup");
        th.find("span").removeClass("caret");
        var current_field = $(".contact_table th[name='" + field + "']").find("div");
        var reserve = "";

        if((field == this.predicate)) {
            reserve = "-";
            current_field.addClass("dropup");
        }
        this.predicate = reserve + field;
        current_field.find("span").addClass("caret");
    }
});
